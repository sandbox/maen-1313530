<?php
/**
 * hook_perm()
 */
function uc_iclear_perm()
{
    return array(
        'uc_iclear resource write any'
    );
}
/**
 * Callback for uc_iclear soap service rpc method acceptOrder
 * 
 * @param type $arg0 
 */
function uc_iclear_accept_order( &$arg0 )
{
    $query = "UPDATE {uc_orders} SET order_status = '%s' WHERE order_id = '%d'";
    //$order = uc_order_load($order->order_id);
    /* Ubercart Status http://api.ubercart.org/api/function/uc_order_update_3/2 */
    switch( intval($arg0->statusID) ) {
        case self::STATUS_ID_CODE_OK:
            // Update the order to paid and set it for deliver
            db_query($query, 'processing', $arg0->basketID);
            //uc_iclear_order("can_update", $arg0, "processing");
            break;
        case self::STATUS_ID_CODE_WAIT:
            // Set to not paid yet and holding the order at time
            db_query($query, 'pending', $arg0->basketID);
            //uc_iclear_order("can_update", $arg0, "pending");
            break;
        case self::STATUS_ID_CODE_CANCEL:
            // Shopping canceled or over timed, so mark order as delete and free the shopping resources in WAWI
            db_query($query, 'canceled', $arg0->basketID);
            //uc_iclear_order("can_update", $arg0, "canceled");
            break;	
    }
    db_query($query);
    
    $return = array( 'return' => array('sessionID' 	=> $req->arg0->sessionID,
                                        'statusID' 		=> self::STATUS_ID_CODE_OK,
                                        'statusMessage' => 'OK',
                                        'basketID' => $req->arg0->basketID,
                                        'shopURL' => url("user/". $arg1->uid ."/orders", array('absolute' => true))
                              ));
    watchdog('uc_iclear', 'Received POST data: %var', array('%var' => var_export($data, TRUE)), WATCHDOG_INFO);
    return $return;
}
