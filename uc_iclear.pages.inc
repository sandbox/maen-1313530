<?php
// $Id: uc_iclear.pages.inc,v 1.2 2010/09/14 15:18:59 jurgenhaas Exp $

/**
 * @file
 * Forms for the uc_iclear module.
 *
 */

/**
 *
 */
function uc_iclear_payment_redirect() {
  $data = _uc_iclear_check_hash();
  if (!$data['verified']) {
    watchdog('uc_iclear', 'Received redirect with invalid GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
    return;
  }
  else {
    watchdog('uc_iclear', 'Received valid redirect with GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
  }
  $order_id = $data['transactionId'];
  if (intval($_SESSION['cart_order']) != $order_id) {
    $_SESSION['cart_order'] = $order_id;
  }
  if (!($order = uc_order_load($order_id))) {
    drupal_goto('cart');
  }
  $gpCode = $data['gpCode'];
  if (_uc_iclear_codeIsOK($gpCode)) {
    // This lets us know it's a legitimate access of the complete page.
    $_SESSION['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  else {
    watchdog('uc_iclear', _uc_iclear_getCodeText($gpCode), array(), WATCHDOG_ERROR);
    drupal_set_message(_uc_iclear_getCodeText($gpCode), 'warning');
    drupal_goto('cart/checkout');
  }
}

/**
 *
 */
function uc_iclear_payment_notify() {
  $data = _uc_iclear_check_hash();
  $result = '400';
  if (!$data['verified']) {
    watchdog('uc_iclear', t('Received notification with invalid GET data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
  }
  else {
    watchdog('uc_iclear', t('Received valid notification with GET data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
    $order_id = $data['transactionId'];
    $order = uc_order_load($order_id);
    if ($order == FALSE) {
      watchdog('uc_iclear', 'Notification attempted for non-existent order.', array(), WATCHDOG_ERROR);
    }
    else {
      $gpCode = $data['gpCode'];
      if (_uc_iclear_codeIsOK($gpCode)) {
        $amount = check_plain($_GET['amount']);
        $context = array(
          'revision' => 'formatted-original',
          'location' => 'paypal-ipn',
        );
        $options = array(
          'sign' => FALSE,
        );
        $comment = t('iclear transaction');
        uc_payment_enter($order_id, 'iclear', $amount, $order->uid, NULL, $comment);
        uc_cart_complete_sale($order);
        uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through iclear.', array('@amount' => uc_price($amount, $context, $options), '@currency' => 'EUR')), 'order', 'payment_received');
        uc_order_comment_save($order_id, 0, t('iclear reported a payment of @amount @currency.', array('@amount' => uc_price($amount, $context, $options), '@currency' => 'EUR')));
        $result = '200';
      }
    }
  }
  print $result;
  exit;
}

/*
 * Obsoleted
 */
// Returns the form elements for the iclear checkout form.
//function uc_iclear_checkout_form($form_state, $order) {
  /* UC Price Hook options */
  /*
	$context = array(
    'revision' => 'formatted-original',
    'location' => 'iclear-form',
  );
  $options = array(
    'sign' => FALSE,
    'thou' => FALSE,
    'dec' => ',',
  );
  */
  /* Giropay stuff */
	/*
  $shipping = 0;
  foreach ($order->line_items as $item) {
    if ($item['type'] == 'shipping') {
      $shipping += $item['amount'];
    }
  }
  $tax = 0;
  if (module_exists('uc_taxes')) {
    foreach (uc_taxes_calculate($order) as $tax_item) {
      $tax += $tax_item->amount;
    }
  }
  $data = array(
    'merchantId' => uc_iclear_var('merchant_id'),
    'projectId' => uc_iclear_var('project_id'),
    'transactionId' => $order->order_id,
    'amount' => uc_price($order->order_total, $context, $options),
    'vwz' => 'Order No. '. $order->order_id .'-'. $order->uid,
    'bankcode' => $order->payment_details['uc_iclear_bankcode'],
    'urlRedirect' => _uc_iclear_url_post('payment_link_redirect') .'?order_id='. $order->order_id,
    'urlNotify' => _uc_iclear_url_post('payment_link_notify') .'?order_id='. $order->order_id .'&amount='. $order->order_total,
  );
  $hashdata = array(
    $data['merchantId'],
    $data['projectId'],
    $data['transactionId'],
    $data['amount'],
    $data['vwz'],
    $data['bankcode'],
    $data['urlRedirect'],
    $data['urlNotify'],
  );
  //$data['hash'] = _uc_iclear_get_hash($hashdata);
  
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }
  */
	//$form[';//_uc_iclear_url_post('start_payment');
/*  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => _uc_iclear_submit(),
    '#action' => $order->uc_iclear_soap_payment_url
  );
  return $form;
}
*/