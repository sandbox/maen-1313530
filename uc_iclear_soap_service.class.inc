<?php
class UC_IClear_SOAP_Service {
	const STATUS_ID_CODE_OK = 0;
	const STATUS_ID_CODE_WAIT = 1;
	const STATUS_ID_CODE_CANCEL = 2;
	/**
	 * 
	 * 
	 * @param string $username
	 * @param string $password
	 */
	/*
	function UsernameToken( $username, $password ){
        // Store username for logging
        $this->Username = $username;
        $auth = new $this->AuthClass( $username, $password, get_class($this) ); // You will have to Define method AuthClass
        if ( $auth->IsValid() ){
            $this->Authenticated = true;
        } else {
            $this->ThrowSoapFault( 'auth' );
        }
    }
    */
	/**
	 * Implements the acceptOrderRequest method from the wsdl schema
	 * 
	 * @author Steffen Stollfuß
	 * 
	 * @param object $req
	 * 
	 * @return void
	 */
	public function acceptOrder( $req )
        {
            /* Your business logic to deal with incoming message goes here
             *
             *
            */
            //var_dump($req);

            /**
             * @var $req stdClass
             * $req is a standard class object with one property:
             * $req->arg0
             * 
             * $req->arg0, an stdClass object, has following properties:
             * requestID - long
             * sessionID - string
             * basketID - string
             * currencyISO - 2 char ISO
             * statusID - long
             * statusMessage - string
             * deliveryAddress - complex type deliveryAddress (stdClass), see docs
             * basketItems - complex type basketItems(array)
             * 
             * 
             * Please note:
             * If basketItems contains only one element, PHP5 renders this property to an object, rather than an array!
             */

            $this->updateUCOrder( $req->arg0 );



            /* Note: We return never another statusID other than 0. */
            $this->return = array(  'sessionID' 	=> $req->arg0->sessionID,
                                    'statusID' 		=> self::STATUS_ID_CODE_OK,
                                    'statusMessage' => 'OK',
                                    'basketID' => $req->arg0->basketID,
                                    'shopURL' => url("user/". $arg1->uid ."/orders", array('absolute' => true))
                              );
            watchdog('uc_iclear', 'Received POST data: %var', array('%var' => var_export($data, TRUE)), WATCHDOG_INFO);
            return $this;
	}
	/**
	 * Update the needed tables to the requested status.
	 * 
	 * 
	 * @param StdClass $order
	 */
	private function updateUCOrder( &$order )
	{
            $query = "UPDATE {uc_orders} SET order_status = '%s' WHERE order_id = '%d'";
            //$order = uc_order_load($order->order_id);
            /* Ubercart Status http://api.ubercart.org/api/function/uc_order_update_3/2 */
            switch( intval($order->statusID) ) {
                case self::STATUS_ID_CODE_OK:
                    // Update the order to paid and set it for deliver
                    db_query($query, 'processing', $order->basketID);
                    //uc_iclear_order("can_update", $order, "processing");
                    break;
                case self::STATUS_ID_CODE_WAIT:
                    // Set to not paid yet and holding the order at time
                    db_query($query, 'pending', $order->basketID);
                    //uc_iclear_order("can_update", $order, "pending");
                    break;
                case self::STATUS_ID_CODE_CANCEL:
                    // Shopping canceled or over timed, so mark order as delete and free the shopping resources in WAWI
                    db_query($query, 'canceled', $order->basketID);
                    //uc_iclear_order("can_update", $order, "canceled");
                    break;	
            }
            db_query($query);
	}
}