<?php
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

/* SOAP Service Class file */
require_once 'uc_iclear_soap_service.class.inc';

$server = new SoapServer( drupal_get_path('module', 'uc_iclear') .'/ressources/wsdl/ICAcceptOrderPort.wsdl',
                                              array("trace" => TRUE,
                                                            "exception" => TRUE,
                                                            //"soap_version" => SOAP_1_2,
                                                            "encoding" => "utf-8",
                                                            "cache_wsdl" => WSDL_CACHE_NONE
                                              ));
try {
            $server->setClass('UC_IClear_SOAP_Service');
            $server->handle($HTTP_RAW_POST_DATA);
} catch( SoapFault $sf )
{
    die( (string) $sf);
}

?>
