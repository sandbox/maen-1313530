<?php
class UC_IClear_SOAP_Basket_Item extends stdClass {
	public $itemID = 0;
	public $title = "";
	public $numOfArticle = 0;
	public $priceNet = 0.0000;
	public $priceGros = 0.0000;
	public $vatRate = 0.0000;
}

class UC_IClear_SOAP_Basket extends stdClass {
    public $shopID = "";
    public $basketID = 0;
    public $currencyISO = "";
    public $languageISO = "";
    public $sessionID = "";
    public $basketItems = array();
    public $deliveryAddress = null;
    public $pageparams = null;
    /**
     * Constructor
     */
    function __construct( &$arg1 ) {
        $this->shopID = uc_iclear_var("shop_id");
        
        $this->basketID =& $arg1->order_id;
        $this->currencyISO =& $arg1->currency;
        $this->languageISO = _uc_iclear_get_country_iso_code($arg1->billing_country);
        $this->sessionID =& $arg1->uc_iclear_soap_session_id;
        
        foreach( $arg1->products as &$product )
        {
            $basketItem = new UC_IClear_SOAP_Basket_Item();
            $basketItem->itemID =& $product->model;
            $basketItem->title =& $product->title;
            $basketItem->numOfArticle =& $product->qty;
            $basketItem->priceNet = sprintf("%.4f",$product->price);
            $basketItem->priceGros = sprintf("%.4f",_uc_iclear_calc_vat_price_for_basket_item($product));
            $basketItem->vatRate = sprintf("%.1f",_uc_iclear_get_tax_rate_for_basket_item($product));
            array_push($this->basketItems, $basketItem);
            unset($basketItem);
        }
        
        foreach ($arg1->line_items as &$line_item )
        {
            if( $line_item["type"] == "shipping" )
            {
                $shippingInfos = new UC_IClear_SOAP_Basket_Item();
                $shippingInfos->itemID = 0;//$line_item["line_item_id"];
                $shippingInfos->title = &$line_item["title"];
                $shippingInfos->numOfArticle = 1;
                $shippingInfos->vatRate = 0.0000;
                $shippingInfos->priceNet = sprintf("%.4f",$line_item["amount"] );
                $shippingInfos->priceGros = sprintf("%.4f", $line_item["amount"] );
                array_push($this->basketItems, $shippingInfos);
                unset($shippingInfos);
            }
        }
        
        $this->deliveryAddress = new UC_IClear_SOAP_Delivery_Address($arg1);
        $this->pageparams = new UC_IClear_SOAP_Page_Params($arg1);
    }
}

class UC_IClear_SOAP_Delivery_Address extends stdClass {
	public $salutationID = ""; // 0 => Ms. 1 => Mr.
	public $firstName = "";
	public $lastName = "";
	public $companyName = "";
	public $deliveryStreet = "";
	public $deliveryStreetNo = ""; // Not a seperate field in drupal ubercart
	public $deliveryZipcode = "";
	public $deliveryCity = "";
	public $deliveryCountryISO = "";
        
        function __construct( &$arg1 ) {
            $this->salutationID = (isset($arg1->extra_fields["ucxf_salutation_delivery"]) ? $arg1->extra_fields["ucxf_salutation_delivery"] : "4"); // 0 => Ms. 1 => Mr. 4 => Company
            $this->firstName =& $arg1->delivery_first_name;
            $this->lastName =& $arg1->delivery_last_name;
            $this->companyName =& $arg1->delivery_company;
            $this->deliveryStreet = $arg1->delivery_street1 . ( !empty($arg1->delivery_street2) ? " -- " . $arg1->delivery_street2 : null );
            $this->deliveryStreetNo = ''; // Not a seperate field in drupal ubercart
            $this->deliveryZipcode =& $arg1->delivery_postal_code;
            $this->deliveryCity =& $arg1->delivery_city;
            $this->deliveryCountryISO = _uc_iclear_get_country_iso_code($arg1->delivery_country);
        }
}

class UC_IClear_SOAP_Page_Params extends stdClass {
	/**
	 * pageParams - used for specify iclear running in an iframe or external window
	 * mode => 'inline'	IFRAME
	 * mode => 'std' EXTERNAL
	 */ 
	public $mode = "std";
        /**
         * An array list of cssparams that are possible to send to IClear to manipulate the request payment iframe.
         * 
         * Note: TODO Documentation
         * 
         * 5
         * Im Falle des Inline Modus wird, falls keine CSS Parameter im Request verwendet werden, geprüft, ob im Shop System ein Ordner
         * css existiert ( im Verzeichnis des Enpunktes) und darin eine iclear.css Datei enthalten ist. Falls diese Datei gefunden wird, werden die
         * Style Angaben darin gefiltert und zulässige Styles als inline Style im iclear Template ausgegeben.
         *
         * 
         * @var array $cssparams
         */
        public $cssparams = null;
	
        public $targetUrlOnCancel = "";
        
	function __construct( &$arg1 ) {
            $this->targetUrlOnCancel = url("user/". $arg1->uid ."/orders", array('absolute' => true));
            
            if(uc_iclear_var('use_payment_iframe') == '1')
            {
		$this->mode = 'inline';
                $payment_iframe_width = uc_iclear_var('payment_iframe_width');
                if( !empty($payment_iframe_width) )
                {
                    $this->cssparams = array( new UC_IClear_Soap_CSS_Params("width", $payment_iframe_width) );
                }
            }
	}
}

class UC_IClear_Soap_CSS_Params extends stdClass {
    public $identifier = "";
    public $value = "";
    function __construct( $identifier, $value) {
        $this->identifier = $identifier;
        $this->value = $value;
    }
}

class UC_IClear_SOAP_Customer_Info extends stdClass {
    public $salutationID = ""; // 0 => Ms. 1 => Mr. 4 => Company
    public $firstName = "";
    public $lastName = "";
    public $companyName = "";
    public $invoiceStreet = "";
    public $invoiceStreetNo = "";
    public $invoiceZipCode = "";
    public $invoiceCity = "";
    public $invoiceCountryISO = "";
    public $DOB = ""; //1941-11-11T00:00:00'; // date formated to YYYY-MM-DDT00:00:00 !!!
    public $emailAddress = "";
    public $phoneNo = "";
    
    function __construct( &$arg1 ) {
        $this->salutationID = (isset($arg1->extra_fields["ucxf_salutation_billing"]) ? $arg1->extra_fields["ucxf_salutation_billing"] : "4"); // 0 => Ms. 1 => Mr. 4 => Company
        $this->firstName =& $arg1->billing_first_name;
        $this->lastName =& $arg1->billing_last_name;
        $this->companyName =& $arg1->billing_company; // optional - leave blank
        $this->invoiceStreet = $arg1->billing_street1 . ( !empty($arg1->billing_street2) ? " -- " . $arg1->billing_street2 : null ) ;
        $this->invoiceStreetNo = '';
        $this->invoiceZipCode =& $arg1->billing_postal_code;
        $this->invoiceCity =& $arg1->billing_city;
        $this->invoiceCountryISO = _uc_iclear_get_country_iso_code($arg1->billing_country);
        $this->DOB = ''; //1941-11-11T00:00:00'; // date formated to YYYY-MM-DDT00:00:00 !!!
        $this->emailAddress =& $arg1->primary_email;
        $this->phoneNo =& $arg1->billing_phone;
    }
}

class UC_IClear_SOAP_Customer extends stdClass {
    public $requestID = "";
    public $sessionID = "";
    public $statusID = UC_IClear_SOAP_Service::STATUS_ID_CODE_OK;
    public $statusMessage = "OK";
    public $customerInfo = null;
    
    function __construct( &$arg1 )
    {
        $this->customerInfo = new UC_IClear_SOAP_Customer_Info($arg1);
    }
        
}