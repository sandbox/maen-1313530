<?php
// $Id: uc_iclear.inc,v 1.2 2010/09/14 15:18:59 jurgenhaas Exp $

/**
 * @file
 * Parameter file for the uc_iclear module.
 *
 * In this file all system parameters from GiroSolutions are collected and
 * get returned to the module according to the current context.
 */

define('UC_ICLEAR_WEBSITE', 'www.iclear.de');
/**
 * Soap Service URL from IClear
 */
define('UC_ICLEAR_SEND_ORDER_PORT','http://services.iclear.de/services/dl/1.0/ICOrderPort');
define('UC_ICLEAR_ACCEPT_ORDER_PORT', 'http://services.iclear.de/services/dl/1.0/ICAcceptOrderPort');

define('UC_ICLEAR_ORDER_PORT_WSDL', drupal_get_path('module', 'uc_iclear'). '/ressources/wsdl/ICOrderPort.wsdl');

/**
 *
 */
function _uc_iclear_url_base() {
  return 'https://'. UC_ICLEAR_WEBSITE;
}

/**
 *
 */
function _uc_iclear_url_post($type) {
  global $base_url;
  switch ($type) {
    case 'start_payment':
      return _uc_iclear_url_base();
    default:
      return $base_url .'/'. _uc_iclear_path_post($type);
  }
}

/**
 *
 */
function _uc_iclear_path_post($type) {
  switch ($type) {
    /* Use for link generation in uc_iclear settings page */
    case 'create_project_form':
      return UC_ICLEAR_WEBSITE;
    /* Link for payment return channel (giropay POST returns) */
    case 'payment_link_redirect':
      return 'cart/uc_iclear/payment/redirect';
    case 'payment_link_notify':
      return 'cart/uc_iclear/payment/notify';
  }
}

// Saves a soap_session_id to an order's data array.
function _uc_iclear_save_soap_data_to_order(&$arg1) {
  // Load up the existing data array.
  $data = _uc_iclear_load_soap_data_from_order($arg1->order_id);
  //$data = db_result(db_query("SELECT data FROM {uc_orders} WHERE order_id = %d", $arg1->order_id));
  //$data = unserialize($data);  
  
  // Stuff that serialized and encrypted CC details into the array.
  $data['uc_iclear_soap_session_id'] = $arg1->uc_iclear_soap_session_id;
  $data['uc_iclear_soap_request_id'] = $arg1->uc_iclear_soap_request_id;
  $data['uc_iclear_soap_client_transfer_success'] = $arg1->uc_iclear_soap_client_transfer_success;
  $data['uc_iclear_soap_payment_url'] = $arg1->uc_iclear_soap_payment_url;
  // Save it again.
  db_query("UPDATE {uc_orders} SET data = '%s' WHERE order_id = %d", serialize($data), $arg1->order_id);
}

// Load a soap_session_id from an order's data array.
function _uc_iclear_load_soap_data_from_order($order_id) {
  // Load up the existing data array.
  $data = db_result(db_query("SELECT data FROM {uc_orders} WHERE order_id = %d", $order_id));
  $data = unserialize($data);
  return $data;
}

/**
 * Generate the IClear Soap Client Transfer Session Id
 */
function _uc_iclear_get_soap_session_id_hash(&$arg1) {
  
  $hashdata = $arg1->uid . $arg1->order_id . $arg1->created;
  return hash_hmac('md5', $hashdata, uc_iclear_var('soap_session_id_salt'));
}

/**
 * Get the current taxrate
 */
function _uc_iclear_get_tax_rate_for_basket_item( &$item )
{
    $res = db_query("SELECT * FROM {term_node} WHERE nid = ". $item->nid);
    
    $taxId = 0;
    while( $termId = db_fetch_array($res) )
    {
        $res2 = db_query("SELECT * FROM {uc_vat_with_taxonomy}");
        while( $temp = db_fetch_array($res2) )
        {
            $buf = unserialize($temp["taxonomy_terms"]);
            foreach( $buf as &$v )
            {
              if( $termId["tid"] == $v )
              {
                  $taxId = $temp["uc_tax_id"];
                  continue 2;
              }
            }
        }
    }
    $res = db_query("SELECT rate FROM {uc_taxes} WHERE id = ". $taxId);
    
    $buf = db_fetch_array($res);
    return $buf["rate"] * 100;
}
/**
 * Calc the Brutto Price
 * @param type $item
 * @return type 
 */
function _uc_iclear_calc_vat_price_for_basket_item($item)
{
    $res = db_query("SELECT * FROM {term_node} WHERE nid = ". $item->nid);
    
    $taxId = 0;
    while( $termId = db_fetch_array($res) )
    {
        $res2 = db_query("SELECT * FROM {uc_vat_with_taxonomy}");
        while( $temp = db_fetch_array($res2) )
        {
            $buf = unserialize($temp["taxonomy_terms"]);
            foreach( $buf as &$v )
            {
              if( $termId["tid"] == $v )
              {
                  $taxId = $temp["uc_tax_id"];
                  continue 2;
              }
            }
        }
    }
    $res = db_query("SELECT rate FROM {uc_taxes} WHERE id = ". $taxId);
    
    $buf = db_fetch_array($res);
    if( $buf !== FALSE )
    {
        return $item->price + $item->price * $buf["rate"];
    }
    return $item->price;
    
}

/**
 * Get the ISO code for a country
 */
function _uc_iclear_get_country_iso_code( &$country_id )
{
    $data = db_result(db_query("SELECT country_iso_code_2 FROM {uc_countries} WHERE country_id = %d", $country_id));
    return $data;
}

/**
 * 
 */
function _uc_iclear_name() {
  return 'iclear';
}

/**
 * 
 */
function _uc_iclear_logo() {
  return theme_image(drupal_get_path('module', 'uc_iclear') .'/ressources/logo.png', _uc_iclear_name(), t('Payment method !name.', array('!name' => _uc_iclear_name())), array(), FALSE);
}

/**
 * 
 */
function _uc_iclear_name_logo() {
  return _uc_iclear_logo();
}

/**
 * 
 */
function _uc_iclear_submit() {
  return t('Submit Order');
}

/**
 *
 */
function _uc_iclear_description() {
  return _uc_iclear_logo() .'<div>'. t('Mit iclear zahlen Sie im Internet einfach, schnell und sicher per Online-Überweisung. Note: Mehr Text nötig !!!') .'</div>';
}

/**
 * Liefert einen Text, der dem Benutzer nach der Bezahlung über iclear angezeigt wird
 *
 * @param integer $gpCode Fehler Code
 * @return string Meldung für den Benutzer
 */
function _uc_iclear_get_soap_code_text($uc_iclear_soap_code) {
	
	switch( intval($uc_iclear_soap_code) ) {
            case 203:
              return t("Missing session ID");
            case 204:
              return t("Missing request ID");
            case 205:
              return t("Missing shop ID");
            case 206:
              return t("Missing basket ID");
            case 207:
              return t("Missing currency");
            case 208:
              return t("Missing numOfArticles");
            case 209:
              return t("Missing priceNet");
            case 210:
              return t("Missing priceGros");
            case 211:
              return t("Missing vatRate");
            case 212:
              return t("Incorrect or missing delivery address ID");
            case 220:
              return t("Cart already transferred to Iclear");
            case 222:
                return t("Testshop isn't available anymore");
            case 301:
              return t("Failed login");
            case 302:
              return t("Not an adult");
            case 401:
              return t("Shop is not active anymore");
            case 402:
              return t("Unknown shop ID");
            case 410:
              return t("Shop is in testmode");
            default:
                return t("Unkown Iclear Soap Error Code");
	}
	return $msg;
}