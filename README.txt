/* $Id: README.txt,v 1.1 2010/06/17 13:15:30 jurgenhaas Exp $ */

-- SUMMARY --

This module allows you to easily and quickly integrate the GiroPay payment method
into your website with an Ubercart online shop.

Before you can make use of GiroPay you need to sign up with a PSP and an Acquirer.
This module is designed to work with http://www.girosolutions.de, a platform of a
PSP called Tineon AG.

More details about GiroPay can be found on http://www.giropay.de.

-- REQUIREMENTS --

* Drupal 6 and Ubercart module 2.0.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure GiroPay in Administer >> Store Administration >> Config Store >> Payment

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* Jürgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de




/* SOAP Stuff need to deliver */
    
    case 'order-details':
        $debug = 123;
    break;
      
    case 'cart-review':
    	$soap_error_title = t('IClear SOAP Client');
    	$soap_error_data = t('IClear data transfer successfully.');
    	
    	$sendOrderB2CParams = new stdClass();
    	$registerCustomerParams = new stdClass();
    	
    	// Setup a basket
    	$basket = new stdClass();
    	$basket->shopID = uc_iclear_var("shop_id");
    	$basket->basketID =& $arg1->order_id;
        $basket->currencyISO = 'EUR';
        $basket->languageISO =& $language->language;
        $basket->sessionID =& $arg1->uc_iclear_soap_session_id;

        $basket->basketItems = array();		

        foreach( $arg1->products as &$product )
        {
                $basketItem = new stdClass();
                $basketItem->itemID =& $product->model;
                $basketItem->title =& $product->title;
                $basketItem->numOfArticle =& $product->qty;
                $basketItem->priceNet = sprintf("%.4f",$product->price);
                $basketItem->priceGros = sprintf("%.4f",_uc_iclear_calc_vat_price_for_basket_item($product));
                $basketItem->vatRate = sprintf("%.1f",_uc_iclear_get_tax_rate_for_basket_item($product));
                array_push($basket->basketItems, $basketItem);
                unset($basketItem);
        }

        /**
         * Add shipping infos for IClear
         */
        $shippingInfos = _uc_iclear_get_shipping_infos($arg1->line_items);
        if( isset($shippingInfos->itemID) ) array_push($basket->basketItems, $shippingInfos);

        /**
         * create the deliveryAddress property of the $basket
         */
        $basket->deliveryAddress = new stdClass();
        $basket->deliveryAddress->salutationID = ''; // 0 => Ms. 1 => Mr.
        $basket->deliveryAddress->firstName =& $arg1->delivery_first_name;
        $basket->deliveryAddress->lastName =& $arg1->delivery_last_name;
        $basket->deliveryAddress->companyName =& $arg1->delivery_company;
        $basket->deliveryAddress->deliveryStreet =& $arg1->delivery_street1 . ( !empty($arg1->delivery_street2) ? " -- " . $arg1->delivery_street2 : null );
        $basket->deliveryAddress->deliveryStreetNo = ''; // Not a seperate field in drupal ubercart
        $basket->deliveryAddress->deliveryZipcode =& $arg1->delivery_postal_code;
        $basket->deliveryAddress->deliveryCity =& $arg1->delivery_city;
        $basket->deliveryAddress->deliveryCountryISO = _uc_iclear_get_country_iso_code($arg1->delivery_country);


        /**
         * pageParams - used for specify iclear running in an iframe or external window
         * mode => 'inline'	IFRAME
         * mode => 'std' EXTERNALust select a shipping opti
         */
         $basket->pageparams = new stdClass();
         $basket->pageparams->mode = ( (uc_iclear_var('use_payment_iframe') == '0') ? 'inline' : 'std' );


         $customerInfo = new stdClass();
        $customerInfo->salutationID = ""; // 0 => Ms. 1 => Mr.
        $customerInfo->firstName =& $arg1->billing_first_name;
        $customerInfo->lastName =& $arg1->billing_last_name;
        $customerInfo->companyName =& $arg1->billing_company; // optional - leave blank
        $customerInfo->invoiceStreet =& $arg1->billing_street1 . ( !empty($arg1->billing_street2) ? " -- " . $arg1->billing_street2 : null ) ;
        $customerInfo->invoiceStreetNo = '';
        $customerInfo->invoiceZipCode =& $arg1->billing_postal_code;
        $customerInfo->invoiceCity =& $arg1->billing_city;
        $customerInfo->invoiceCountryISO = _uc_iclear_get_country_iso_code($arg1->billing_country);;
        $customerInfo->DOB = ''; //1941-11-11T00:00:00'; // date formated to YYYY-MM-DDT00:00:00 !!!
        $customerInfo->emailAddress =& $arg1->primary_email;
        $customerInfo->phoneNo =& $arg1->billing_phone;
        /* We don't ask for that atm.
        $customerInfo->bankAccountID = '1234567890';
        $customerInfo->bankBIC = 'MANSDE66XXX';
        $customerInfo->bankBLZ = '67050505';
        $customerInfo->bankCustomerName = 'Tester Test';
        $customerInfo->bankIBAN = '';
        $customerInfo->bankName = 'Sparkasse Rhein Neckar Nord';
        */

        /*
         * create the request object and assign properties
         */
        $registerCustomerParams->requestID = "";
        $registerCustomerParams->sessionID = "";
        $registerCustomerParams->statusID = UC_IClear_SOAP_Service::STATUS_ID_CODE_OK;
        $registerCustomerParams->statusMessage = "OK";
        $registerCustomerParams->customerInfo =& $customerInfo;

         // create the soap call parameter
         $sendOrderB2CParams = array('arg0' => &$basket);
         $registerCustomerParams = array( "arg0" => $registerCustomerParams);
// Prepare Stuff to delivery over soap

         try
         {
                $soap_client = new SoapClient(drupal_get_path('module', 'uc_iclear'). '/ressources/wsdl/ICOrderPort.wsdl',
                                                              array( "encoding" => "utf-8",
                                                                     "cache_wsdl" => WSDL_CACHE_NONE,
                                                                     "trace" => TRUE,
                                                                     "exception" => TRUE
                                                              )
                                                );
                $soap_result = $soap_client->sendOrderB2C($sendOrderB2CParams);

                $registerCustomerParams["arg0"]->requestID =& $soap_result->return->requestID;
                $registerCustomerParams["arg0"]->sessionID =& $soap_result->return->sessionID;
                $soap_client->registerCustomer($registerCustomerParams);
                $soap_error_data = $soap_error_data . " Please go to URL: " . $soap_result->return->iclearURL;
                $soap_transfer_success = TRUE;

         } 
         catch( SoapFault $soap_fault )
         {
                $soap_transfer_success = FALSE;
                $soap_error_data = (string) $soap_fault;
         }
        $arg1->uc_iclear_soap_client_transfer_success =& $soap_transfer_success;
        $arg1->uc_iclear_soap_request_id =& $soap_result->return->requestID;
        $arg1->uc_iclear_soap_payment_url =& $soap_result->return->iclearURL;
        uc_iclear_order("save", $arg1, $arg2);
    	//if (isset($arg1->payment_details['uc_iclear_bankcode']) && !empty($arg1->payment_details['uc_iclear_bankcode'])) {
        //$review[] = array('title' => t('Bankcode of your bank'), 'data' => check_plain($arg1->payment_details['uc_iclear_bankcode']));
      //}
        $review[] = array('title' => $soap_error_title, 'data' => $soap_error_data );
      return $review;